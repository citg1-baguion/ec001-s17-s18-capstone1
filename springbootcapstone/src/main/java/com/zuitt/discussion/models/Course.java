package com.zuitt.discussion.models;

import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Set;

// Marks this java object as a representation of an entity/record from the database table "posts".
@Entity
//Designate the table name related to the model.
@Table(name = "courses")
public class Course {

//    Properties
//    Indicates the primary key
    @Id
//    id will be auto-incremented.
    @GeneratedValue
    private int id;

//    Class properties that represents table column in a relational database are annotated as @Column
    @Column
    private String name;

    @Column
    private String description;

    @Column
    private double price;

    @OneToMany(mappedBy = "course")
    private Set<CourseEnrollment> enrollees;

    @Column
    private boolean isActive;

    //    establishes the relationship of the property to the "post" model
    @ManyToOne
//    sets the relationship to this property and user_id column in the database to the primary key of the user model.
    @JoinColumn(name="user_id", nullable = false)
    private User user;

//    Constructors
//    Default constructors are required when retrieving data from the database.
    public Course(){}

    public Course(String name, String description, double price,boolean isActive) {
        this.name = name;
        this.description = description;
        this.isActive = isActive;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<CourseEnrollment> getEnrollees() {return enrollees;}

    public void setEnrollees(Set<CourseEnrollment> enrollees) {this.enrollees = enrollees;}

    public boolean isActive() {return isActive;}

    public void setActive(boolean active) {isActive = active;}

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
