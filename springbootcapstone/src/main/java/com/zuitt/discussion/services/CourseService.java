// Service is an interface that exposes the methods of an implementation whose details have been abstracted away.
package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Course;
import org.springframework.http.ResponseEntity;

public interface CourseService {

    void createCourse(Course course, String stringToken);

    Iterable<Course> getCourses();

    ResponseEntity updateCourse(int id, Course course, String stringToken);

    ResponseEntity deleteCourse(int id, String stringToken);

    ResponseEntity archiveUnarchiveCourse(int courseId, String stringToken);

    Iterable<Course> getActiveCourses();

    Course getCourse(int courseId);

}
