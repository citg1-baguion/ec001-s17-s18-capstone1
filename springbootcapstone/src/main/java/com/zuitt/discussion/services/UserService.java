package com.zuitt.discussion.services;

import com.zuitt.discussion.models.User;

import java.util.Optional;

public interface UserService {

    void createUser(User user);

    Optional<User> findByUsername(String username);
}
