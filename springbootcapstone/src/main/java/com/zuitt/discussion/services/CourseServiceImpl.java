//contains the "business logic" concerned with a particular object in the class.

package com.zuitt.discussion.services;

import com.zuitt.discussion.config.JwtToken;
import com.zuitt.discussion.models.Course;
import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.CourseRepository;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
//Will allow us to use the CRUD methods inherited from the CRUDRepository
@Service
public class CourseServiceImpl implements CourseService {

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    @Override
    public void createCourse(Course course, String stringToken) {
        User user = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        Course newCourse = new Course();

        newCourse.setName(course.getName());
        newCourse.setDescription(course.getDescription());
        newCourse.setPrice(course.getPrice());
        newCourse.setActive(true);
        newCourse.setUser(user);

        courseRepository.save(newCourse);
    }

    @Override
    public Iterable<Course> getCourses() {
        return courseRepository.findAll();
    }

    @Override
    public Course getCourse(int courseId) {return courseRepository.findById(courseId).get();}

    @Override
    public Iterable<Course> getActiveCourses() {return courseRepository.findByIsActive(true);}

    @Override
    public ResponseEntity updateCourse(int id, Course course, String stringToken) {
        Course courseForUpdate = courseRepository.findById(id).get();
        String user = courseForUpdate.getUser().getUsername();
        String authenticatedUsername = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUsername.equals(user)) {
            courseForUpdate.setName(course.getName());
            courseForUpdate.setDescription(course.getDescription());
            courseForUpdate.setPrice(course.getPrice());

            courseRepository.save(courseForUpdate);
            return new ResponseEntity<>("Course updated successfully", HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity<>("You are not authorized to edit this course.", HttpStatus.UNAUTHORIZED);
        }
    }

    @Override
    public ResponseEntity archiveUnarchiveCourse(int courseId, String stringToken) {
        Course courseForArchive = courseRepository.findById(courseId).get();
        String user = courseForArchive.getUser().getUsername();
        String authenticatedUsername = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUsername.equals(user)) {
            courseForArchive.setActive(!courseForArchive.isActive());
            courseRepository.save(courseForArchive);

            return new ResponseEntity<>("Course archived/unarchived successfully.", HttpStatus.OK);
        }

        return new ResponseEntity<>("You are not authorized to archived this course.", HttpStatus.UNAUTHORIZED);
    }


    @Override
    public ResponseEntity deleteCourse(int id, String stringToken) {
        Course courseForDelete = courseRepository.findById(id).get();

        String user = courseForDelete.getUser().getUsername();
        String authenticatedUsername = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUsername.equals(user)) {
            courseRepository.deleteById(id);
            return new ResponseEntity<>("Course deleted successfully.", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You are not authorized to delete this course.", HttpStatus.UNAUTHORIZED);
        }
    }

}
