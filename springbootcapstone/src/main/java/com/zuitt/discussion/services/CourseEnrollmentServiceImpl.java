package com.zuitt.discussion.services;

import com.zuitt.discussion.config.JwtToken;
import com.zuitt.discussion.models.Course;
import com.zuitt.discussion.models.CourseEnrollment;
import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.CourseEnrollmentRepository;
import com.zuitt.discussion.repositories.CourseRepository;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class CourseEnrollmentServiceImpl implements CourseEnrollmentService {

    @Autowired
    private CourseEnrollmentRepository courseEnrollmentRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private JwtToken jwtToken;

    @Override
    public String enrollCourse(String stringToken, int courseId) {
        User user = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        Course course = courseRepository.findById(courseId).get();
        CourseEnrollment courseEnrollment = new CourseEnrollment();

        if(course.isActive()) {
            courseEnrollment.setCourse(course);
            courseEnrollment.setUser(user);
            courseEnrollment.setDateTimeEnrolled(LocalDateTime.now());

            courseEnrollmentRepository.save(courseEnrollment);

            return "Enrollment successful";
        } else {
            return "Course unarchived. You cannot enroll to this course.";
        }
    }
}


