package com.zuitt.discussion.services;

public interface CourseEnrollmentService {

    String enrollCourse(String stringToken, int courseId);
}

