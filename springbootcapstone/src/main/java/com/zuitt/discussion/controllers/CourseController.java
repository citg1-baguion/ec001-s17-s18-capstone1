//Classes where services are mapped to their corresponding endpoints and HTTP methods via @RequestMapping annotation.

package com.zuitt.discussion.controllers;

import com.zuitt.discussion.models.Course;
import com.zuitt.discussion.services.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
//Enables cross origin request via @CrossOrigin.
@CrossOrigin
public class CourseController {

    @Autowired
    CourseService courseService;

//    Create Post
    @RequestMapping(value = "/courses", method = RequestMethod.POST)
    public ResponseEntity<Object> createCourse(@RequestBody Course course, @RequestHeader("Authorization") String stringToken) {
    courseService.createCourse(course, stringToken);
    return new ResponseEntity("Course created successfully.", HttpStatus.CREATED);
}

    @RequestMapping(value = "/courses", method = RequestMethod.GET)
    public ResponseEntity<Object> getCourses() {
        return new ResponseEntity(courseService.getCourses(), HttpStatus.OK);
    }

    @RequestMapping(value = "courses/{courseId}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updateCourse(@PathVariable int courseId, @RequestBody Course course, @RequestHeader("Authorization") String stringToken) {
        return courseService.updateCourse(courseId, course, stringToken);
    }

    @RequestMapping(value = "/courses/{courseId}", method = RequestMethod.GET)
    public ResponseEntity<Object> getCourse(@PathVariable int courseId) {
        return new ResponseEntity<>(courseService.getCourse(courseId), HttpStatus.OK);
    }

    @RequestMapping(value = "/courses/{courseId}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteCourse(@PathVariable int courseId, @RequestHeader("Authorization") String stringToken) {
        return courseService.deleteCourse(courseId, stringToken);
    }

    @RequestMapping(value = "/courses/archive-unarchive/{courseId}", method = RequestMethod.PUT)
    public ResponseEntity<Object> archiveUnarchiveCourse(@PathVariable int courseId, @RequestHeader("Authorization") String stringToken) {
        return courseService.archiveUnarchiveCourse(courseId, stringToken);
    }

    @RequestMapping(value = "/courses/active", method = RequestMethod.GET)
    public ResponseEntity<Object> getActiveCourses() {
        return new ResponseEntity<>(courseService.getActiveCourses(), HttpStatus.OK);
    }


}
